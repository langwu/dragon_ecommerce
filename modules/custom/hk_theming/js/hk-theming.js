(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.fixMenu = {
    attach: function(context, settings){
      $( "li.menu-item--expanded" ).mouseover(function() {
	$(this).find('ul').fadeIn( 500 );
      });
      $( "li.menu-item--expanded" ).mouseout(function() {
	$(this).find('ul').fadeOut( 500 );
      });
    }
  }

  Drupal.behaviors.alterHtml = {
    attach: function(context, settings) {
      $('.product--variation-field--variation_price__1').insertAfter($('.field--name-field-country'))
    }
  }

  Drupal.behaviors.register = {
    attach: function(context, settings) {
      $('.user-register-form input.select-registration-roles').click(function() {
        if($(this).attr('value') == 'btc'){
          $( ".user-register-form div.field--type-string").css('display', 'none');
        }else{
          $( ".user-register-form div.field--type-string").css('display', 'block');
        }
      });
    }
  }

})(jQuery, Drupal, drupalSettings);
