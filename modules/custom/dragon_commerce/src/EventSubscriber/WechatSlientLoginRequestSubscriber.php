<?php

namespace Drupal\dragon_commerce\EventSubscriber;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\RedirectDestinationInterface;
use Drupal\Core\Url;
use Drupal\Core\Routing\UrlGeneratorInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Routing\RouteMatch;

/**
 * Redirects users when access is denied.
 *
 * Anonymous users are taken to the wechat auth page when attempting to access the
 * pages need permission.
 */
class WechatSlientLoginRequestSubscriber implements EventSubscriberInterface {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The redirect destination service.
   *
   * @var \Drupal\Core\Routing\RedirectDestinationInterface
   */
  protected $redirectDestination;

  /**
   * Constructs a new redirect subscriber.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Routing\UrlGeneratorInterface $url_generator
   *   The URL generator.
   * @param \Drupal\Core\Routing\RedirectDestinationInterface $redirect_destination
   *   The redirect destination service.
   */
	public function __construct(AccountInterface $account, UrlGeneratorInterface $url_generator, RedirectDestinationInterface $redirect_destination) {
		$this->account = $account;
		$this->redirectDestination = $redirect_destination;
	}

	/**
	 * Redirects users when access is denied.
	 *
	 * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
	 *   The event to process.
	 */
	public function onKernelRequestCheckRedirect(GetResponseEvent $event) {
		$request = clone $event->getRequest();
		$route_name = RouteMatch::createFromRequest($request)->getRouteName();
		if($route_name == 'commerce_checkout.form' && $this->account->isAuthenticated()){
			$agent = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
			if(strpos($agent, 'MicroMessenger') !== FALSE && !isset($_COOKIE['openid'])){
				$appid = 'wx587973e7787fbb01';
				$scope = 'snsapi_base';
				$callback = Url::fromRoute('dragon_commerce.authorize', ['code_type' => 'wx_js'], ['absolute' => TRUE])->toString();
				$destination = Url::fromUserInput($this->redirectDestination->get())->setAbsolute()->toString();
				$auth_url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=$appid&redirect_uri=" . urlencode($callback) . "&response_type=code&scope=$scope&state=$destination#wechat_redirect";
				\Drupal::logger('capital-test')->debug(print_r($auth_url, true));

				$response = new TrustedRedirectResponse($auth_url);
				$event->setResponse($response);
			}
		}
	}

	/**
	 * {@inheritdoc}
	 */
	public static function getSubscribedEvents() {
		// Run after AuthenticationSubscriber (necessary for the 'user' cache
		// context; priority 300) and MaintenanceModeSubscriber (Page redirect
		// should not be polluted by maintenance mode-specific behavior; priority
		// 30), but before DynamicPageCacheSubscriber (to avoid Dynamic Page Cache;
		// priority 27) and before our other event subscriber (priority 28).
		$events[KernelEvents::REQUEST][] = ['onKernelRequestCheckRedirect', 28];
		return $events;
	}

}

