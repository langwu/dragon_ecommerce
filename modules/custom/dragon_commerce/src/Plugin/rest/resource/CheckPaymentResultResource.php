<?php

namespace Drupal\dragon_commerce\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\commerce_order\Entity\Order;

/**
 * Provides a payment status .
 *
 * @RestResource(
 *   id = "payment_result",
 *   label = @Translation("Check Payment Result"),
 *   uri_paths = {
 *     "create" = "/check/payment/result"
 *   }
 * )
 */
class CheckPaymentResultResource extends ResourceBase {

  /**
   * Responds to payment result GET requests.
   *
   * @return \Drupal\rest\ResourceResponse
   */
  public function get() {
    $order_id = \Drupal::request()->get('order_id');
    $order = Order::load($order_id);
    $status = $order->isPaid();
    $result = ['status' => $status];
    $response = new ResourceResponse($result);
    $response->getCacheableMetadata()->setCacheMaxAge(0);
    return $response;
  }

}
