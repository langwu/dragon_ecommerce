<?php

namespace Drupal\dragon_commerce\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\node\Entity\Node;

/**
 * Field handler to provide a list of roles.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("package_route")
 */
class PackageRouteField extends FieldPluginBase {

  /**
   *
   */
  public function query() {
  }

  /**
   *
   */
  public function render(ResultRow $values) {
    $trackingInfo = self::getTrackingInfo();
    \Drupal::logger('test')->debug(print_r($trackingInfo, true));
    return '123123123123';
  }

  private function create_uuid() {
    $chars = md5(uniqid(mt_rand(), true));
    $uuid = substr ( $chars, 0, 8 ) . '-'
        . substr ( $chars, 8, 4 ) . '-'
        . substr ( $chars, 12, 4 ) . '-'
        . substr ( $chars, 16, 4 ) . '-'
        . substr ( $chars, 20, 12 );
    return $uuid ;
  }

  private function getTrackingInfo() {
    $partnerID = "PLSHGSjEQvC9"; // 顾客编码
    $checkword="ehpA2SxjBT0M0QY74VktZbPSx41xO9gt"; // 平台校验码
    $serviceCode = "EXP_RECE_SEARCH_ROUTES";
    $msgData = json_encode([
      "language" => "0", //0中文，1英文，2繁体
      "trackingType"=> "1", // 1对应运单号，2对应订单号
      "trackingNumber" => ["SF7444407228423"], 
      "methodType" => "1" // 标准定制路由
    ]);
    $requestID = self::create_uuid();
    $timestamp = time();
    $msgDigest = base64_encode(md5((urlencode($msgData .$timestamp. $checkword)), TRUE));
    $post_data = array(
      'partnerID' => $partnerID,
      'requestID' => $requestID,
      'serviceCode' => $serviceCode,
      'timestamp' => $timestamp,
      'msgDigest' => $msgDigest,
      'msgData' => $msgData
    );
    //沙箱环境的地址
    $CALL_URL_BOX = "http://sfapi-sbox.sf-express.com/std/service";
    //生产环境的地址
    $CALL_URL_PROD = "https://sfapi.sf-express.com/std/service";
    $result = _dragon_commerce_http_post($CALL_URL_BOX, $post_data, ['Content-type' => 'application/x-www-form-urlencoded;charset=utf-8']);
    return $result;
  }

}
