<?php

namespace Drupal\dragon_commerce\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 *
 */
class ParseController extends ControllerBase {

  /**
   *
   */
  public function content() {
    $state = isset($_GET['state']) ? $_GET['state'] : 'internal:/';
    $destination = ($state == 'null') ? 'internal:/' : $state;
    $code = isset($_GET['code']) ? $_GET['code'] : '';
    $path = Url::fromUri($destination)->setAbsolute()->toString();

    $appid="wx587973e7787fbb01";  //应用签名
    $appkey="dbbcf1d06accc297834650c0cd7370d0";  //应用签名
    $url='https://api.weixin.qq.com/sns/oauth2/access_token?appid='.$appid.'&secret='.$appkey.'&code='.$code.'&grant_type=authorization_code';

    $data=file_get_contents($url);
    $data=json_decode($data);

    $openid=$data->openid;
    \Drupal::logger('capital-test-openid')->debug(print_r($openid, true));

    setcookie("openid", $openid);
    return new RedirectResponse($path);
  }

}
