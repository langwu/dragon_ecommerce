<?php

function dragon_commerce_views_data()
{

	$data['commerce_order']['package_route'] = [
		'title' => t('Package Route'),
		'field' => [
			'id' => 'package_route',
		],
	];

	return $data;
}
