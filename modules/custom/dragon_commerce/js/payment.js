(function ($, Drupal, drupalSettings) {

  'use strict';
  
  Drupal.behaviors.checkStatus = {
    attach: function(context, settings){
      if(!window.isLoaded) {
        window.isLoaded = true;
        var order_id = drupalSettings.dragon_commerce.js.orderId;
        // var id = getCookie('id');
        setTimeout(function() {
          var t = setInterval(function() {
            $.ajax({
              url: location.pathname,
              type: 'GET',
              success: function(data) {
                if (!data.includes('Please scan the QR-Code')) {
                  clearInterval(t);
                  window.location.href = '/user/' + id + 'orders';
                }
              }
            });
          }, 3000)
        }, 15000)
        // var t = setInterval(function() {
        //   $.ajax({
        //     url: '/check/payment/result',
        //     type: 'GET',
        //     data: {_format: 'json', order_id: order_id, timestamp: $.now()},
        //     success: function(data) {
        //       var pay_status = data['status'];
        //       if(pay_status) {
        //         window.location.href = '/user/' + id + 'orders';
        //       }
        //     }
        //   });
        // }, 2000)
        function getCookie(key) {
          var arr,reg=new RegExp("(^| )"+key+"=([^;]*)(;|$)");
          if(arr=document.cookie.match(reg)){
            return unescape(arr[2]);
          } else {
            return '';
          }
        }
      }
    }
  }

})(jQuery, Drupal, drupalSettings);